# Use the official Maven image as the build environment
FROM maven:3.8.3-openjdk-11 AS builder

# Set the working directory in the container
WORKDIR /app

# Copy the pom.xml file to the container
COPY pom.xml .

# Resolve and download project dependencies
RUN mvn dependency:go-offline -B

# Copy the project source code to the container
COPY src ./src

# Build the project
RUN mvn package -DskipTests

# Use a lightweight JRE base image for the final image
FROM openjdk:11-jre-slim

# Set the working directory in the container
WORKDIR /app

# Copy the built JAR file from the builder stage to the final image
COPY --from=builder /app/target/*.jar ./app.jar

# Expose the desired port (replace 8080 with your application's actual port)
EXPOSE 8080

# Define the command to run your application
CMD ["java", "-jar", "app.jar"]
